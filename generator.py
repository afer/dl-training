'''
Based on https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
'''
from pathlib import Path
import numpy as np
import pandas as pd
from tensorflow.python.keras.utils.data_utils import Sequence
from tensorflow.keras.utils import to_categorical
# from image_io import load_tiled_image
from image_io import load_annotations
from preprocessing import extract_cell
from preprocessing import standard_norm


class dfImageLabel(Sequence):
    def __init__(self, annot_df, aug, batch_size=32, dim=(117,117), n_channels = 5, n_classes = 2,
                 preprocess=None, shuffle=True):
        """Generator for training image-label pairs.

        Arguments:
            paths: an iterable the location of the annotations df
            preprocess: a single callable or tuple of callables (one for each
                file of pair); None specifies the default `standard_norm`
        """

        self.annotation_df = annot_df
        self.list_IDs = list(self.annotation_df.index)
        self.paths = self.annotation_df['filename']
        self.labels=self.annotation_df['annotation']

        self.aug = aug
        self.dim = dim
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.batch_size = batch_size
        self.shuffle=shuffle

        if preprocess is None:
            self.preprocess = standard_norm
        else:
            self.preprocess = preprocess

        # assert (all([callable(f) for f in self.preprocess])), \
        #     'preprocess argument specified incorrectly'

        # Initialise ordering
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor(len(self.annotation_df) / float(self.batch_size)))

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __getitem__(self, idx):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[idx*self.batch_size:(idx+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def __data_generation(self, list_IDs_temp):
      'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
      # Initialization
      X = np.empty((self.batch_size, *self.dim, self.n_channels))
      y = np.empty((self.batch_size), dtype=int)

      # Generate data
      for i, ID in enumerate(list_IDs_temp):
          assert ID in self.annotation_df.index.values, 'The sample ID is out of bounds: ID ' + str(ID) + \
              ' self.annot len: ' + str(len(self.annotation_df.index.values))
          # Store class
          y[i] = self.labels.loc[ID]

          # Store sample
          X[i,] = self.aug.apply(extract_cell(self.annotation_df.loc[ID]), y[i])[0]


      return X, to_categorical(y, num_classes=self.n_classes)
