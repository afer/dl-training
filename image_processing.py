import numpy as np
 
def get_center_distances(mask):
    # Find center of rectangle that encompasses the cell
    xaxis = np.sum(mask, axis=0).astype(bool).tolist()
    yaxis = np.sum(mask, axis=1).astype(bool).tolist()
    upper_lim = xaxis.index(True)
    lower_lim = len(xaxis)-xaxis[::-1].index(True)
    left_lim = yaxis.index(True)
    right_lim = len(yaxis)-yaxis[::-1].index(True)

    cell_x_center = int(round((left_lim+right_lim)/2))
    cell_y_center = int(round((upper_lim+lower_lim)/2))
    
    image_x_center = int(len(xaxis)/2)
    image_y_center = int(len(yaxis)/2)

    # Find the distance in pixels between cell center and image center
    x_distance = image_x_center - cell_x_center
    y_distance = image_y_center - cell_y_center

    return(x_distance, y_distance)

def scroll_matrix(matrix, distance, axis=0):
    # Similar functionaly to shift function from scikitimage
    '''
    Function to
    matrix::ndarray Matrix to scroll over
    distance::int Distance to move the whole matrix
    axis::int 0 for rows and 1 for cols
    '''

    axis_len = matrix.shape[axis]
    new_order = np.mod(np.add(range(axis_len), distance), axis_len)
    new_matrix = matrix[:, new_order] if axis else matrix[new_order, :]

    return(new_matrix)

def center_img_and_mask(img, mask):
    x,y = get_center_distances(mask)

    images_list = []
    for ndarray in (img,mask):
        tmp_img = scroll_matrix(ndarray,-x,0)
        images_list.append(scroll_matrix(tmp_img, -y, 1))

    return(images_list)

def normalise_marrays(marray_list):
    normalised_marrays = []
    for marray in marray_list:
        max_tmp = marray.max(fill_value=0)

        min_tmp = marray.min(fill_value=max_tmp)

        used_range = max_tmp-min_tmp
        normalised_marray = (marray.filled(min_tmp) -min_tmp)/used_range
        normalised_marrays.append(normalised_marray)
    return(np.array(normalised_marrays))

# Functions to improve the cnn's accuracy
def shuffle_dataset(input_data):
    input_imgs, input_lbls = input_data
    dsize = input_imgs.shape[0]
    new_sequence = np.random.permutation(dsize)
    img_new_order = input_imgs[new_sequence,:,:]
    lbl_new_order = input_lbls[new_sequence,:]
    out_dset = (img_new_order, lbl_new_order)
    return(out_dset)

