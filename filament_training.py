from pathlib import Path
import numpy as np
import pandas as pd
from generator import dfImageLabel
from image_io import load_annotations
from tensorflow.keras import datasets, models, callbacks, Input, Model, applications
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, concatenate, Lambda, Flatten, Dense
from augmentation import Augmenter

# Parameters
params = {'dim': (117,117),
          'batch_size': 32,
          'n_classes': 2,
          'n_channels': 5,
          'shuffle': True,
          'preprocess': None}

dfs_paths = ['annotations.tsv']

# Check that all required images exist
assert all([Path(p).exists() for p in dfs_paths]), 'some of the specified tables do not exist'

train_aug = Augmenter(xy_out=117, probs={'rotate': 0.97, 'noise': 0.00001}, ignore_label=True)

# We load our dataframe with the labels, cell ids and location, and shuffle it
annot_df = pd.concat([load_annotations(annot) for annot in dfs_paths],
                                       ignore_index=True).sample(frac=1).reset_index(drop=True)

train_ratio = 0.75

dsize = len(annot_df)
split = int(np.floor(dsize*train_ratio)) # TODO add eval ratio

partition = {'train':annot_df.iloc[:split], 'validation':annot_df.iloc[split:]}
labels = {str(idx):annot_df.iloc[idx]['annotation'] for idx in annot_df.index.values}

training_generator = dfImageLabel(partition['train'], train_aug, **params)
validation_generator = dfImageLabel(partition['validation'], train_aug, **params)

#Model architecture

img_height=117
img_width=117
img_channels=5
inputs = Input((img_height, img_width, img_channels))
s = Lambda(lambda x: x / 255) (inputs)

c1 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same',  input_shape=(117, 117, 5), data_format='channels_last') (s)
c2 = Conv2D(16, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c1)
p1 = MaxPooling2D((2, 2)) (c2)

c3 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p1)
c4 = Conv2D(32, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c3)
p2 = MaxPooling2D((2, 2)) (c4)

c5 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p2)
c6 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c5)
p3 = MaxPooling2D((2, 2)) (c6)

c7 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (p3)
c8 = Conv2D(64, (3, 3), activation='elu', kernel_initializer='he_normal', padding='same') (c7)
p4 = MaxPooling2D(pool_size=(2, 2)) (c8)

d5 = Flatten() (p4)
d5 = Dense(64, activation='relu') (d5)
outputs = Dense(2, activation='softmax') (d5)

checkpointer = callbacks.ModelCheckpoint('2020_02_model.h5', verbose=1, save_best_only=True)
# reduce_loss = callbacks.ReduceLROnPlateau(monitor='acc',
#                                                     factor=0.1, patience=10, verbose=0, mode='auto',
#                                                     min_delta=0.0001, cooldown=0, min_lr=0)
early_stop = callbacks.EarlyStopping(monitor='acc', min_delta=0,
                           patience=300, verbose=0, mode='auto',
                           baseline=None)

chosen_callbacks = [checkpointer, early_stop] # reduce_loss

model = Model(inputs=[inputs], outputs=[outputs])
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])
# Train model on dataset
history = model.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    use_multiprocessing=True,
                              workers=4, epochs=500, callbacks=chosen_callbacks)

from matplotlib import pyplot as plt
plt.plot(history.history['acc'], label='accuracy')
plt.plot(history.history['val_acc'], label = 'val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1.05])
plt.legend(loc='lower right')
plt.show()
