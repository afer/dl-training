from matplotlib import pyplot as plt
from preprocessing import extract_cell
from preprocessing import standard_norm

def visualise_first_nfigs(arrays_list, n, nstacks):
    plt.figure(figsize=(10,10))
    for i in range(n):
        for j in range(nstacks):
            plt.subplot(n,nstacks,i*nstacks+j+1)
            plt.xlabel('Row: ' + str(i) + ' ' + 'Z: ' + str(j+1))

            plt.xticks([])
            plt.yticks([])
            plt.grid(False)
            plt.imshow(arrays_list[i,:,:,j])
    plt.show()

def extract_and_plot(generator, n, nstacks):
    plt.figure(figsize=(10,10))
    for i in range(n):
        for j in range(nstacks):
            plt.subplot(n,nstacks,i*nstacks+j+1)
            plt.xlabel('Row: ' + str(i) + ' ' + 'Z: ' + str(j+1))

            plt.xticks([])
            plt.yticks([])
            plt.grid(False)
            plt.imshow(generator.aug.apply(extract_cell(generator.annotation_df.loc[i]), generator.labels.loc[i])[0][:,:,i])
    plt.show()
